#include <stdio.h>4
#include <string.h>
#include "contiki.h"
#include "cfs/cfs.h"
#include "cfs/cfs-coffee.h"
#include "lib/random.h"
#include "net/rime/rime.h"
#include "net/rime/collect.h"
#include "dev/leds.h"
#include "dev/button-sensor.h"
#include "net/netstack.h"
#include "net/rime/rime.h"


static struct collect_conn tc;

PROCESS(combined_test, "Combined Test");
AUTOSTART_PROCESSES(&combined_test);

#define FILENAME "testfile"
/* Formatting is needed if the storage device is in an unknown state; 
   e.g., when using Coffee on the storage device for the first time. */
#ifndef NEED_FORMATTING
#define NEED_FORMATTING 0
#endif

//////////////////////////////////////////////////////////////////////// COFFEE CFS
static int
file_test(const char *filename, char *msg)
{
	int fd;
	int r;
	char message[32];
	char buf[64];
	strncpy(message, "First Message", sizeof(message) - 1);
	message[sizeof(message) - 1] = '\0';
	strcpy(buf,message);
	/*First message is to test if the write will succeed*/
	printf("Write Test: Will write \"%s\" to file \"%s\"\n",buf,FILENAME);

  /* Obtain a file descriptor for the file, capable of handling both 
     reads and writes. */
	fd = cfs_open(FILENAME, CFS_WRITE | CFS_APPEND | CFS_READ);
	if(fd < 0) {
		printf("failed to open %s\n", FILENAME);
		return 0;
	}
  /*Write message to Filesystem*/
	r = cfs_write(fd, message, sizeof(message));
	if(r != sizeof(message)) {
		printf("failed to write %d bytes to %s\n",
				(int)sizeof(message), FILENAME);
		cfs_close(fd);
		return 0;
	}
	cfs_close(fd);
	printf("Write Test: Successfully wrote \"%s\" to \"%s\" wrote %d bytes\n ",message,FILENAME,r);

	strcpy(buf,"fail");
	fd = cfs_open(FILENAME, CFS_READ);
	if(fd < 0) {
		printf("failed to open %s\n", FILENAME);
		return 0;
	}
	r = cfs_read(fd, buf, sizeof(message));
	if(r != sizeof(message)) {
		printf("failed to write %d bytes to %s\n",(int)sizeof(message), FILENAME);
		cfs_close(fd);
		return 0;
	}
      /* compare with the original message to see if the message was read 
      correctly, if it reads fail then it will print fail*/
	printf("Read Test: Read \"%s\" from \"%s\"\n",buf, FILENAME);
	cfs_close(fd);
     /*Append test */
	strcpy(message,"Append Something");
	fd = cfs_open(FILENAME, CFS_WRITE | CFS_APPEND | CFS_READ);
	if(fd < 0) {
		printf("failed to open %s\n", FILENAME);
		return 0;
	}
	r = cfs_write(fd, message, sizeof(message));
	cfs_close(fd);
	printf("Append Test: Successfully \"%s\" to \"%s\" \n ",message,FILENAME);
	strcpy(buf,"fail");
	fd = cfs_open(FILENAME, CFS_READ);
	if(fd < 0) {
		printf("failed to open %s\n", FILENAME);
		return 0;
	}
	cfs_read(fd,buf,sizeof(message));
	printf("Read First Part \"%s\"\n",buf);
       /*seek test*/
	if(cfs_seek(fd, sizeof(message), CFS_SEEK_SET) == -1) {
		printf("seek failed\n");
		cfs_close(fd);
		return 0;
	}
  //cfs_seek(fd, sizeof(message), CFS_SEEK_SET);
    /*if the seek fails then the second message will not 
    be the same as the last message write to the file*/
	cfs_read(fd,buf,sizeof(message));
	printf("Read Second Part: \"%s\"\n",buf);
	cfs_close(fd);
	return 1;
}
//////////////////////////////////////////////////////////////////////// END COFFEE CFS

//////////////////////////////////////////////////////////////////////// COLLECT
static void
recv(const linkaddr_t *originator, uint8_t seqno, uint8_t hops)
{
  printf("Sink got message from %d.%d, seqno %d, hops %d: len %d '%s'\n",
	 originator->u8[0], originator->u8[1],
	 seqno, hops,
	 packetbuf_datalen(),
	 (char *)packetbuf_dataptr());

	// When recieve message, test the file function.
  if(file_test(FILENAME, "Message Recieved") == 0) {
    printf("file test 1 failed\n");
  }
  printf("test succeed\n");

}

static const struct collect_callbacks callbacks = { recv };

//////////////////////////////////////////////////////////////////////// END COLLECT


PROCESS_THREAD(combined_test, ev, data)
{

  static struct etimer periodic;
  static struct etimer et;

  PROCESS_BEGIN();

#if NEED_FORMATTING
  cfs_coffee_format();
#endif

  /* Ensure that we will be working with a new file. */
  //cfs_remove(FILENAME);

	collect_open(&tc, 130, COLLECT_ROUTER, &callbacks);

	  if(linkaddr_node_addr.u8[0] == 1 &&
	     linkaddr_node_addr.u8[1] == 0) {
		printf("I am sink\n");
		collect_set_sink(&tc, 1);
	  }

	  /* Allow some time for the network to settle. */
	  etimer_set(&et, 120 * CLOCK_SECOND);
	  PROCESS_WAIT_UNTIL(etimer_expired(&et));

	  while(1) {

	    /* Send a packet every 30 seconds. */
	    if(etimer_expired(&periodic)) {
	      etimer_set(&periodic, CLOCK_SECOND * 30);
	      etimer_set(&et, random_rand() % (CLOCK_SECOND * 30));
	    }

	    PROCESS_WAIT_EVENT();


	    if(etimer_expired(&et)) {
	      static linkaddr_t oldparent;
	      const linkaddr_t *parent;

	      printf("Sending\n");
	      packetbuf_clear();
	      packetbuf_set_datalen(sprintf(packetbuf_dataptr(),
					  "%s", "Hello") + 1);
	      collect_send(&tc, 15);

	      parent = collect_parent(&tc);
	      if(!linkaddr_cmp(parent, &oldparent)) {
		if(!linkaddr_cmp(&oldparent, &linkaddr_null)) {
		  printf("#L %d 0\n", oldparent.u8[0]);
		}
		if(!linkaddr_cmp(parent, &linkaddr_null)) {
		  printf("#L %d 1\n", parent->u8[0]);
		}
		linkaddr_copy(&oldparent, parent);
	      }
	    }

	  }

  PROCESS_END();
}


