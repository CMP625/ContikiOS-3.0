/*
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */

#include "contiki.h"
#include "contiki-lib.h"
#include "contiki-net.h"
#include <stdio.h>

#include "cfs/cfs.h"
#include "cfs/cfs-coffee.h"


#include <string.h>

#define DEBUG DEBUG_PRINT
#include "net/ip/uip-debug.h"

#define UIP_IP_BUF   ((struct uip_ip_hdr *)&uip_buf[UIP_LLH_LEN])

#define MAX_PAYLOAD_LEN 120

#define FILENAME "test"
/* Formatting is needed if the storage device is in an unknown state; 
   e.g., when using Coffee on the storage device for the first time. */
#ifndef NEED_FORMATTING
#define NEED_FORMATTING 0
#endif

static struct uip_udp_conn *server_conn;

PROCESS(udp_server_process, "UDP server process");
AUTOSTART_PROCESSES(&resolv_process,&udp_server_process);

///////////////////////////////////////////////////////////////////////////////
static int
file_test(const char *filename, char *msg)
{
	int fd;
	int r;
	char message[32];
	char buf[64];
	strncpy(message, "First Message", sizeof(message) - 1);
	message[sizeof(message) - 1] = '\0';
	strcpy(buf,message);
	/*First message is to test if the write will succeed*/
	printf("Write Test: Will write \"%s\" to file \"%s\"\n",buf,FILENAME);

  /* Obtain a file descriptor for the file, capable of handling both 
     reads and writes. */
	fd = cfs_open(FILENAME, CFS_WRITE | CFS_APPEND | CFS_READ);
	if(fd < 0) {
		printf("failed to open %s\n", FILENAME);
		return 0;
	}
  /*Write message to Filesystem*/
	r = cfs_write(fd, message, sizeof(message));
	if(r != sizeof(message)) {
		printf("failed to write %d bytes to %s\n",
				(int)sizeof(message), FILENAME);
		cfs_close(fd);
		return 0;
	}
	cfs_close(fd);
	printf("Write Test: Successfully wrote \"%s\" to \"%s\" wrote %d bytes\n ",message,FILENAME,r);

	strcpy(buf,"fail");
	fd = cfs_open(FILENAME, CFS_READ);
	if(fd < 0) {
		printf("failed to open %s\n", FILENAME);
		return 0;
	}
	r = cfs_read(fd, buf, sizeof(message));
	if(r != sizeof(message)) {
		printf("failed to write %d bytes to %s\n",(int)sizeof(message), FILENAME);
		cfs_close(fd);
		return 0;
	}
      /* compare with the original message to see if the message was read 
      correctly, if it reads fail then it will print fail*/
	printf("Read Test: Read \"%s\" from \"%s\"\n",buf, FILENAME);
	cfs_close(fd);
     /*Append test */
	strcpy(message,"Append Something");
	fd = cfs_open(FILENAME, CFS_WRITE | CFS_APPEND | CFS_READ);
	if(fd < 0) {
		printf("failed to open %s\n", FILENAME);
		return 0;
	}
	r = cfs_write(fd, message, sizeof(message));
	cfs_close(fd);
	printf("Append Test: Successfully \"%s\" to \"%s\" \n ",message,FILENAME);
	strcpy(buf,"fail");
	fd = cfs_open(FILENAME, CFS_READ);
	if(fd < 0) {
		printf("failed to open %s\n", FILENAME);
		return 0;
	}
	cfs_read(fd,buf,sizeof(message));
	printf("Read First Part \"%s\"\n",buf);
       /*seek test*/
	if(cfs_seek(fd, sizeof(message), CFS_SEEK_SET) == -1) {
		printf("seek failed\n");
		cfs_close(fd);
		return 0;
	}
  //cfs_seek(fd, sizeof(message), CFS_SEEK_SET);
    /*if the seek fails then the second message will not 
    be the same as the last message write to the file*/
	cfs_read(fd,buf,sizeof(message));
	printf("Read Second Part: \"%s\"\n",buf);
	cfs_close(fd);
  /* Release the internal resources held by Coffee for
     the file descriptor. */
	cfs_remove(FILENAME);
	fd = cfs_open(FILENAME, CFS_READ);
	if(fd != -1) {
		printf("ERROR: could read from memory\n");
		return 0;
	}
	printf("Successfully removed file\n");
	return 1;
}

///////////////////////////////////////////////////////////////////////////////

/*---------------------------------------------------------------------------*/
static void
tcpip_handler(void)
{
  static int seq_id;
  char buf[MAX_PAYLOAD_LEN];
	  if(file_test(FILENAME, "The first test") == 0) {
    printf("file test 1 failed\n");
  }
  printf("test succeed\n");

  if(uip_newdata()) {
    ((char *)uip_appdata)[uip_datalen()] = 0;
    PRINTF("Server received: '%s' from ", (char *)uip_appdata);
    PRINT6ADDR(&UIP_IP_BUF->srcipaddr);
    PRINTF("\n");

    uip_ipaddr_copy(&server_conn->ripaddr, &UIP_IP_BUF->srcipaddr);
    PRINTF("Responding with message: ");
    sprintf(buf, "Hello from the server! (%d)", ++seq_id);
    PRINTF("%s\n", buf);

    uip_udp_packet_send(server_conn, buf, strlen(buf));
    /* Restore server connection to allow data from any node */
    memset(&server_conn->ripaddr, 0, sizeof(server_conn->ripaddr));
  
}
}
/*---------------------------------------------------------------------------*/
static void
print_local_addresses(void)
{
  int i;
  uint8_t state;

  PRINTF("Server IPv6 addresses: ");
  for(i = 0; i < UIP_DS6_ADDR_NB; i++) {
    state = uip_ds6_if.addr_list[i].state;
    if(uip_ds6_if.addr_list[i].isused &&
       (state == ADDR_TENTATIVE || state == ADDR_PREFERRED)) {
      PRINT6ADDR(&uip_ds6_if.addr_list[i].ipaddr);
      PRINTF("\n");
    }
  }
}
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(udp_server_process, ev, data)
{
#if UIP_CONF_ROUTER
  uip_ipaddr_t ipaddr;
#endif /* UIP_CONF_ROUTER */


  PROCESS_BEGIN();

#if NEED_FORMATTING
  cfs_coffee_format();
#endif

  PRINTF("UDP server started\n");

#if RESOLV_CONF_SUPPORTS_MDNS
  resolv_set_hostname("contiki-udp-server");
#endif

#if UIP_CONF_ROUTER
  uip_ip6addr(&ipaddr, 0xaaaa, 0, 0, 0, 0, 0, 0, 0);
  uip_ds6_set_addr_iid(&ipaddr, &uip_lladdr);
  uip_ds6_addr_add(&ipaddr, 0, ADDR_AUTOCONF);
#endif /* UIP_CONF_ROUTER */

  print_local_addresses();

  server_conn = udp_new(NULL, UIP_HTONS(3001), NULL);
  udp_bind(server_conn, UIP_HTONS(3000));

  while(1) {
    PROCESS_YIELD();
    if(ev == tcpip_event) {
      tcpip_handler();
    }
  }

  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
